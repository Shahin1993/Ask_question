
<?php
error_reporting(0);
include("vendor/autoload.php");
use App\question\question;
use App\answer\answer;

$obj=new  question();
$row=$obj->show($_GET['id']);

$obj1=new  answer();
$result=$obj1->getAlldata();

?>


<?php
session_start();
if(empty($_SESSION['userid'])){
    $_SESSION['Message']="<div class='error'>Please login and try again </div>";

    header("location:login.php");
}

?>

<?php include('includs/header.php'); ?>


<div class="breadcrumbs">
		<section class="container">
			<div class="row">
                <?php

                if(isset($_SESSION['Message'])){
                    echo $_SESSION['Message'];
                    unset ($_SESSION['Message']);
                }
                ?>
				<div class="col-md-12">
					<h1><?php echo $row['title']; ?></h1>
				</div>
				<div class="col-md-12">
					<div class="crumbs">
						<a href="index.php">Home</a>
						<span class="crumbs-span">/</span>
						<a href="#">Questions</a>
						<span class="crumbs-span">/</span>
						<span class="current"> </span>
					</div>
				</div>
			</div><!-- End row -->
		</section><!-- End container -->
	</div><!-- End breadcrumbs -->
	
	<section class="container main-content">
		<div class="row">
			<div class="col-md-9">
				<article class="question single-question question-type-normal">
					<h2>
						<a href="single_question.html"><?php echo $row['title']; ?></a>
					</h2>
					<div class="question-inner">
						<div class="color"></div>
						<div class="question-desc">
							<p><?php echo $row['q_description']; ?>  </p>
						</div>


						<div  style="=color: green;font-weight: bold;"><h2> Total Answers:
                            <?php
                            foreach($result as $count)
                            {
                            }
                            include("config.php");

                            $statement= $db->prepare("SELECT * FROM tbl_answer WHERE q_id=?");
                            $statement->execute(array($count['q_id']));
                            $row= $statement->rowCount();
                            echo $row;
                            ?>
                            </h2>
                        </div>
					</div>
				</article>


                <?php
                    foreach($result as $answer)
                { ?>
				
				<div id="commentlist" class="page-content">
					<div class="boxedtitle page-title"><h2>Answers


                             <span class="color"></span> </h2></div>
					<ol class="commentlist clearfix">
					    <li class="comment">
					        <div class="comment-body comment-body-answered clearfix"> 
					            <div class="avatar">
<!--                                    --><?php
//                                    $gravatarMd5 = md5($answer['email'] );
//
//                                    ?>
                                    <?php


                                    include ('config.php');
                                    $statement= $db->prepare("SELECT * FROM user_tbl WHERE id=?");
                                    $statement->execute(array($answer['u_id']) );
                                    $result= $statement->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($result as $man)
                                    {
                                        $user=$man;
                                    }
                                             $gravatarMd5 = md5($user['email'] );
                                    ?>
                                    <?php


                                    ?>
                                    <img alt="" src="http://www.gravatar.com/avatar/<?php echo $gravatarMd5; ?>">
                                </div>
					            <div class="comment-text">
					                <div class="author clearfix">
					                	<div class="comment-author"><a href="#"><?php echo $user['username']; ?> </a></div>
					                	<div class="comment-vote">
						                	<ul class="question-vote">
						                		<li><a href="#" class="question-vote-up" title="Like"></a></li>
						                		<li><a href="#" class="question-vote-down" title="Dislike"></a></li>
						                	</ul>
					                	</div>
					                	<span class="question-vote-result">+1</span>
					                	<div class="comment-meta">
					                        <div class="date"><i class="icon-time"></i>

                                                <?php echo $answer['a_date']; ?>

                                                </div>
					                    </div>
					                    <a class="comment-reply" href="#"><i class="icon-reply"></i>Reply</a> 
					                </div>
                                    <div class="text"><p><?php echo $answer['u_answer'];?>  </p></div>

									<div class="question-answered question-answered-done"><i class="icon-ok"></i>Best Answer</div>
                                    <div class="question-answered question-answered-done"><i class="icon-ok"></i>
                                  <?php
                                  if(($_SESSION['userid']['id'])==$answer['u_id'])
                                  { ?>
                                            <a href="answer_edit.php?id=<?php echo $answer['id']; ?>" >Edit</a>
                                  <?php }?>
                                        </div>
					            </div>
					        </div>
                        </li>
					</ol><!-- End commentlist -->
				</div><!-- End page-content -->
                <?php } ?>

				<div   class="comment-respond page-content clearfix">
				    <div class="boxedtitle page-title"><h2>Answer Now


                        </h2></div>
				    <form action="store/answer.php" method="post" id="commentform" class="comment-form">

				        <div id="respond-textarea">
				            <p>

                                <textarea id="summernote" name="u_answer"  cols="58" rows="8" ></textarea>

                                <script type="text/javascript">



                                    $(document).ready(function() {
                                        $('#summernote').summernote({
                                            height: "200px"
                                        });
                                    });

                                </script>
				            </p>
				        </div>
				        <p class="form-submit">
				        	<input name="submit" type="submit" id="submit" value="Post your answer" class="button small color">
				        </p>
                        <input type="hidden" name="question_id" value="<?php echo $_GET['id']; ?>">

				    </form>
				</div>
				

			</div><!-- End main -->

	
	 <?php include('includs/footer.php'); ?>