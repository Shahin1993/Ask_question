<?php

namespace App\question;
use PDO;
 session_start();
class question
{
    private $q_title;
    private $q_description;

    public function addquestion($data1 = '')
    {
        $u_id = $_SESSION['userid'];
        $this->q_title = $data1['q_title'];
        $this->q_description = $data1['q_description'];
        $this->username = $u_id['username'];
        $this->email = $u_id['email'];
        return $this;
    }

    public function stoequestion()
    {
        try {
            $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
            $query = "INSERT INTO tbl_question(title,q_description,p_date,username,email) values(:t, :q, :d, :u, :e)";
            $stmt = $db->prepare($query);
            $status = $stmt->execute(
                array(
                    ':t' => $this->q_title,
                    ':q' => $this->q_description,
                    ':d' => date('Y-m-d'),
                    ':u' => $this->username,
                    ':e' => $this->email,


                ));
            if ($status) {
                $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size: 30px;'>Question add successfully.</div>";

                header("location: ../ask_question.php");
            } else {
                echo "something is wrong";
            }

        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }

    public function getquestion()
    {

        try {


            $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');

            $query = "SELECT * FROM `tbl_question` ORDER BY id DESC ";
            $stmt = $db->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();


        } catch (\PDOException $e) {
            echo "Error" . $e->getMessage();
        }
        return $data;
        //return $result;
    }

    public function show($id = '')
    {

        $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
        $query = "SELECT * FROM `tbl_question` WHERE id=$id";
        $stmt = $db->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }

    public function q_delete($id = '')
    {
        $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
        $statement = $db->prepare("DELETE FROM tbl_question WHERE id=?");
        $status = $statement->execute(array($id));

        if ($status) {
            $_SESSION['Message'] = "<div class='success'>Deleted  successfully.</div>";

            header("location: question_view.php");
        }


    }
}
?>