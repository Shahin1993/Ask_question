<?php

 namespace App\answer;
 use PDO;
class answer
{
    private $u_answer;
    private $question_id;

    public function getAlldata()
    {

        try {


            $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
            $query = "SELECT * FROM `tbl_answer` where q_id=? ";
            $stmt = $db->prepare($query);
            $stmt->execute(array($_GET['id']));
            $data = $stmt->fetchAll();


        } catch (PDOException $e) {
            echo "Error" . $e->getMessage();
        }
        return $data;
    }

    public function addPost($data1 = '')
    {
        $this->u_answer = $data1['u_answer'];
        $this->question_id = $data1['question_id'];


        return $this;

    }

    public function storeData()
    {
        session_start();
        $row = $_SESSION['userid'];
        try {

            $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
            $query = "INSERT INTO tbl_answer(u_answer,q_id,u_id,a_date) values(:a, :q_id, :u_name,  :d)";
            $stmt = $db->prepare($query);
            $status = $stmt->execute(
                array(
                    ':a' => $this->u_answer,
                    ':q_id' => $this->question_id,
                    ':u_name' => $row['id'],
                    ':d' => date('Y-m-d h:m:s'),


                ));
            if ($status) {
                $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size:30px;'>Answer post successfully.</div>";

                header("location: ../single_question.php?id=" . $this->question_id);
            } else {
                echo "something is wrong";
            }

        } catch (PDOException $e) {
            echo 'Error' . $e->getMessage();
        }
    }

    public function showanswer($id = '')
    {

        $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
        $query = "SELECT * FROM `tbl_answer` WHERE id=?";
        $stmt = $db->prepare($query);
        $stmt->execute(array($id));
        $data = $stmt->fetch();
        return $data;
    }

    public function updateanswer($id = '')
    {
        $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');
        $query = "UPDATE tbl_answer SET u_answer WHERE id=?";
        $stmt = $db->prepare($query);
        $status = $stmt->execute(
            array($id));
        if ($status) {
            $_SESSION['Message'] = "<div style='color: green;font-weight: bold;font-size: 30px;'>Update successfully.</div>";

            header("location:../index.php");
        } else {
            echo "something is wrong";
        }

    }

    public function count()
    {

        $db = new PDO('mysql:host=localhost;dbname=bitm_1', 'root', '');

        $statement= $db->prepare("SELECT * FROM tbl_answer WHERE q_id=?");
        $statement->execute(array($this->question_id));
        $result2= $statement->rowCount();
        //echo $result2;


        return $result2;
    }
}
?>